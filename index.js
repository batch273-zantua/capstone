

class Product{
	constructor(pname, price){
		this.pname = pname;
		this.price = price;
		this.isActive = true;
	}

	archive(product){

		this.isActive = false;
		return this;
	}
	updatePrice(price){
		this.price = price;
		return this;
	}
}

class Cart{
	constructor(){
		this.contents = [];
		this.totalAmount =0;
	}

	addToCart(product, qty){

		this.contents.push({"product": product, "quantity":qty});
		return this;
	}
	showCartContents(){
		return this.contents;

	}
	updateProductQuantity(prod, newQty){

		this.contents.forEach(product => {
			if(product.product == prod){
				product.quantity = newQty;
			}
		})
		return this;

	}
	clearContents(customer){
		this.contents = [];
		return this;
	}
	computeTotal(){
		let total = 0;
		this.contents.forEach(product => {
			total += product.product.price * product.quantity;
		})
		this.totalAmount = total;
		return total;


	}
	
}
class Customer{
	constructor(email){
		this.email = email;
		this.orders = [];
		this.cart = new Cart;
	}

	checkOut(){

		this.orders.push({"contents": this.cart.contents, "totalAmount" : this.cart.computeTotal()});
		return this;

	}
}

const customer1 = new Customer("john@mail.com");
const customer2 = new Customer("jane@mail.com");
const product1 = new Product("Soap", 100);
const product2 = new Product("Shampoo", 200);
customer1.cart.addToCart(product1, 3);
customer2.cart.addToCart(product1, 2);
customer2.cart.addToCart(product2, 10);
console.log(customer1);
console.log(customer2);



